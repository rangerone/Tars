//
//  LoginVC.swift
//  Tars
//
//  Created by yishain chen on 2017/12/16.
//  Copyright © 2017年 Ranger Technologies. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoginVC: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate {
    
    var loadingActivityIndicator = LoadingAnimatorUtilities.createLoadingAnimator()
    var loginButton = GIDSignInButton()
    var backgroundView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black

        GIDSignIn.sharedInstance().clientID = "745310593012-0vu61s3afnqbujfqdrq98pq8jshnujmn.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        let profileScope = "https://www.googleapis.com/auth/userinfo.profile"
        let emailScope = "https://www.googleapis.com/auth/userinfo.email"
        
        GIDSignIn.sharedInstance().scopes.append(contentsOf: [profileScope, emailScope])
        self.setupLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupLayout() {
        backgroundView.image = UIImage(named: "LaunchScreen")
        backgroundView.clipsToBounds = true
        view.addSubview(backgroundView)
        
        loginButton = GIDSignInButton()
        loginButton.style = .wide
        loginButton.addTarget(self, action: #selector(loginButtonPressed), for: .touchUpInside)
        view.addSubview(loginButton)
        loginButton.isHidden = true
        
        signInSilently()
        
        loadingActivityIndicator.center = view.center
        loadingActivityIndicator.color = .offWhite
        view.addSubview(loadingActivityIndicator)
        
        backgroundView.snp.makeConstraints { make in
            make.top.left.right.bottom.equalTo(view)
        }
        
        loginButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(view.center.x)
            make.bottom.equalTo(view.snp.bottom).offset(-170)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        guard error == nil else {
            print("\(error.localizedDescription)")
            loginButton.isHidden = false
            loadingActivityIndicator.stopAnimating()
            self.loginButton.isHidden = false
            return
        }
        
        guard let accessToken = user.authentication.accessToken else { return } // Safe to send to the server
        UserDefaults.standard.setValue(accessToken, forKey: "user_auth_token")
            
        loadingActivityIndicator.stopAnimating()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        appDelegate.didFinishAuthenticatingUser()

        
    }
    
    @objc func loginButtonPressed() {
        loginButton.isHidden = true
        loadingActivityIndicator.startAnimating()
    }
    
    func signInSilently() {
        GIDSignIn.sharedInstance().signInSilently()
    }
    
    func handleSignIn(url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }

}

//
//  ProCastListVC.swift
//  Tars
//
//  Created by yishain chen on 2017/12/10.
//  Copyright © 2017年 Ranger Technologies. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

let TarsPodcastListCellIdentifier = "TarsPodcastListCellIdentifier"

class PodcastListVC: UIViewController {
    
    var podcastList = UITableView(frame: CGRect.zero)
    var podcastListVM: PodcastListVM
    let disposeBag = DisposeBag()

    init(podcastListVM: PodcastListVM) {
        self.podcastListVM = podcastListVM
        super.init(nibName: nil, bundle: nil)
        self.setupLayout()
        self.bindViewModel()
        self.configureTableView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func configureTableView() {
        podcastList.register(PodcastListCell.self, forCellReuseIdentifier: TarsPodcastListCellIdentifier)
        podcastList.rowHeight = 100
        podcastList.separatorStyle = .none
    }
    
    func setupLayout()  {
        podcastList.backgroundColor = UIColor.black
        view.addSubview(podcastList)
        
        podcastList.snp.makeConstraints { make in
            make.top.left.right.bottom.equalTo(view)
        }
        
    }

    func bindViewModel()  {
        
        let viewWillAppear = rx.methodInvoked(#selector(UIViewController.viewWillAppear(_:)))
            .asDriver(onErrorJustReturn: [])
        
        
        let input = PodcastListVM.Input(viewWillAppear: viewWillAppear,
                                        selection: podcastList.rx.itemSelected.asDriver())
        
        let output = podcastListVM.transform(input: input)
        
        output.podcastLists?
            .drive(podcastList.rx.items(cellIdentifier: TarsPodcastListCellIdentifier, cellType: PodcastListCell.self)) {
            (row, elememt, cell) in
            cell.configure(withPodcast: elememt)
            }.disposed(by: disposeBag)
        
        output.selectedPodcast?.asObservable().subscribe(onNext: { (selectedPodcast) in
            
            let selectedPodcastInteractor = SelectedPodcastInteractor()
            let selectedPodcastVM = SelectedPodcastVM(interactor: selectedPodcastInteractor)
            let selectedPodcastVC = SelectedPodcastVC(selectedPodcast: selectedPodcast, selectedPodcastVM: selectedPodcastVM)
            self.navigationController?.pushViewController(selectedPodcastVC, animated: true)
        }).disposed(by: disposeBag)
    }
}

//
//  PodcastListCell.swift
//  Tars
//
//  Created by yishain chen on 2017/12/16.
//  Copyright © 2017年 Ranger Technologies. All rights reserved.
//

import UIKit

class PodcastListCell: UITableViewCell {

    open var podcastNameLabel: UILabel
    open var podcastTimeLabel: UILabel
    open var podcastIconImageView: UIImageView
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        podcastNameLabel = UILabel()
        podcastTimeLabel = UILabel()
        podcastIconImageView = UIImageView()
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = UIColor.black
        contentView.addSubview(podcastNameLabel)
        contentView.addSubview(podcastTimeLabel)
        contentView.addSubview(podcastIconImageView)
        
        podcastNameLabel.textColor = UIColor.white
        podcastNameLabel.numberOfLines = 2
        
        podcastTimeLabel.textColor = UIColor.white
        
        podcastIconImageView.snp.makeConstraints { make in
            make.centerY.equalTo(contentView.snp.centerY)
            make.left.equalTo(contentView.snp.left).offset(10)
            make.height.width.equalTo(80)
        }
        
        podcastNameLabel.snp.makeConstraints { make in
            make.top.equalTo(contentView).offset(10)
            make.left.equalTo(podcastIconImageView.snp.right).offset(10)
            make.right.equalTo(contentView.snp.right).offset(-10)
        }
        
        podcastTimeLabel.snp.makeConstraints { make in
            make.bottom.equalTo(contentView.snp.bottom).offset(-10)
            make.right.equalTo(contentView.snp.right).offset(-10)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(withPodcast podcast: Podcast) {
        podcastIconImageView.image = podcast.podcastAlbumArt
        podcastNameLabel.text = podcast.artistName
        podcastTimeLabel.text = "-11:01"
        
    }
    
}

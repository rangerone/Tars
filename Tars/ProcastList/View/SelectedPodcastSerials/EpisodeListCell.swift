//
//  EpisodeListCell.swift
//  Tars
//
//  Created by yishain chen on 2017/12/22.
//  Copyright © 2017年 Ranger Techlologies. All rights reserved.
//

import UIKit

class EpisodeListCell: UITableViewCell {
    
    open var episodeNameLabel: UILabel
    open var episodeIconImageView: UIImageView
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        episodeNameLabel = UILabel()
        episodeIconImageView = UIImageView()
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = UIColor.black
        contentView.addSubview(episodeIconImageView)
        
        episodeNameLabel.textColor = UIColor.white
        episodeNameLabel.numberOfLines = 0
        contentView.addSubview(episodeNameLabel)
        
        episodeIconImageView.snp.makeConstraints { make in
            make.centerY.equalTo(contentView.snp.centerY)
            make.left.equalTo(contentView.snp.left).offset(10)
            make.height.width.equalTo(80)
        }
        
        episodeNameLabel.snp.makeConstraints { make in
            make.top.equalTo(self).offset(10)
            make.left.equalTo(episodeIconImageView.snp.right).offset(10)
            make.right.equalTo(self).offset(-10)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(withPodcast episode: Episode, albumImage: UIImage) {
        episodeIconImageView.image = albumImage
        episodeNameLabel.text = episode.title
        
    }
    
}


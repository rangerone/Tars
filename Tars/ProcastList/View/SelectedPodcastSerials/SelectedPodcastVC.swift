//
//  SelectedPodcastVC.swift
//  Tars
//
//  Created by yishain chen on 2017/12/22.
//  Copyright © 2017年 Ranger Techlologies. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

let TarsEpisodeListCellIdentifier = "TarsEpisodeListCellIdentifier"

class SelectedPodcastVC: UIViewController {

    var selectedPodcastVM: SelectedPodcastVM
    var episodeList = UITableView(frame: CGRect.zero)
    var episodeListHeaderView = EpisodeListHeaderView()
    let disposeBag = DisposeBag()
    let selectedPodcast: Podcast
    
    init(selectedPodcast: Podcast, selectedPodcastVM: SelectedPodcastVM) {
        self.selectedPodcast = selectedPodcast
        self.selectedPodcastVM = selectedPodcastVM
        super.init(nibName: nil, bundle: nil)
        self.configureTableView()
        self.setupLayout()
        self.bindViewModel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func bindViewModel()  {
        let viewWillAppear = rx.methodInvoked(#selector(UIViewController.viewWillAppear(_:)))
            .asDriver(onErrorJustReturn: [])
        
        self.selectedPodcastVM.selectedPodcast.value = self.selectedPodcast
        
        let input = SelectedPodcastVM.Input(viewWillAppear: viewWillAppear,
                                        selection: episodeList.rx.itemSelected.asDriver())
        
        let output = selectedPodcastVM.transform(input: input)
        
        output.episodeLists?
            .drive(episodeList.rx.items(cellIdentifier: TarsEpisodeListCellIdentifier, cellType: EpisodeListCell.self)) {
                [unowned self] (row, elememt, cell) in
                cell.configure(withPodcast: elememt, albumImage: self.selectedPodcast.podcastAlbumArt!)
            }.disposed(by: disposeBag)
        
        output.selectedEpisode?.asObservable().subscribe(onNext: { (selectedEpisode) in
            
            let episodeDetailVC = EpisodeDetailVC(selectedEpisode: selectedEpisode, albumImage: self.selectedPodcast.podcastAlbumArt!)
            self.navigationController?.pushViewController(episodeDetailVC, animated: true)
        }).disposed(by: disposeBag)
        
        episodeListHeaderView.subscribeButton.rx.tap
            .subscribe(onNext: { [unowned self] _ in
            
                
            })
            .disposed(by: disposeBag)
    
    }
    
    func setupLayout()  {
        
        episodeListHeaderView.podcastAlbumImageView.image = self.selectedPodcast.podcastAlbumArt

        view.addSubview(episodeListHeaderView)
        view.addSubview(episodeList)
        
        episodeListHeaderView.snp.makeConstraints { make in
            make.top.left.right.equalTo(view)
            make.height.equalTo(300)
        }
        
        episodeList.snp.makeConstraints { make in
            make.top.equalTo(episodeListHeaderView.snp.bottom)
            make.left.right.equalTo(view)
            make.bottom.equalTo(view).offset(-50)
        }
        
    }
    
    private func configureTableView() {
        episodeList.register(EpisodeListCell.self, forCellReuseIdentifier: TarsEpisodeListCellIdentifier)
        episodeList.backgroundColor = UIColor.black
        episodeList.sectionHeaderHeight = 250
        episodeList.rowHeight = 100
        episodeList.separatorStyle = .none
        
    }
    
    
    

}

//
//  EpisodeListHeaderView.swift
//  Tars
//
//  Created by yishain chen on 2017/12/22.
//  Copyright © 2017年 Ranger Techlologies. All rights reserved.
//

import UIKit

class EpisodeListHeaderView: UIView {
    var podcastAlbumImageView = UIImageView()
    var subscribeButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.darkGray
        self.alpha = 0.7
        
        podcastAlbumImageView.layer.cornerRadius = 75
        self.addSubview(podcastAlbumImageView)
        
        subscribeButton.layer.cornerRadius = 20
        subscribeButton.setTitle("Subscribe", for: .normal)
        subscribeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        subscribeButton.backgroundColor = UIColor.init(red: 0.0, green: 0.55, blue: 0.94, alpha: 0.8)

        subscribeButton.titleLabel?.textColor = UIColor.white
        
        self.addSubview(subscribeButton)
        
        
        podcastAlbumImageView.snp.makeConstraints { [unowned self] (make) in
            make.top.equalTo(self).offset(30)
            make.width.height.equalTo(150.0)
            make.centerX.equalTo(self)
        }
        
        subscribeButton.snp.makeConstraints { [unowned self] (make) in
            make.top.equalTo(podcastAlbumImageView.snp.bottom).offset(40)
            make.width.equalTo(130.0)
            make.height.equalTo(50.0)
            make.centerX.equalTo(self)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

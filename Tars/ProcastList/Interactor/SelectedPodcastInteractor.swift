//
//  SelectedPodcastInteractor.swift
//  Tars
//
//  Created by yishain chen on 2017/12/23.
//  Copyright © 2017年 Ranger Techlologies. All rights reserved.
//

import UIKit
import RxSwift

class SelectedPodcastInteractor: NSObject {

    func getEpisodes(selectedPod: Podcast) -> Observable<[Episode]> {
        return Observable<[Episode]>.create({ observer in
            RSS.shared.rssFeed = selectedPod.podcastFeed
            RSS.shared.getEpisodes { (episodes) in
                observer.onNext(episodes!)
            }
            return Disposables.create()
        })
    } 
}

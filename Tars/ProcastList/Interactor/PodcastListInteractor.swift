//
//  PodcastListInteractor.swift
//  Tars
//
//  Created by yishain chen on 2017/12/17.
//  Copyright © 2017年 Ranger Techlologies. All rights reserved.
//

import UIKit
import RxSwift

class PodcastListInteractor: NSObject {

    func getPodcasts() ->  Observable<[Podcast]> {
        return Observable<[Podcast]>.create({ observer in
            iTunes.shared.getPodcasts { (podcasts) in
                observer.onNext(podcasts!)
            }
            return Disposables.create()
        })
    }
}

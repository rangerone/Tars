
//
//  SelectedPodcastVM.swift
//  Tars
//
//  Created by yishain chen on 2017/12/23.
//  Copyright © 2017年 Ranger Techlologies. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SelectedPodcastVM: NSObject {

    var interactor: SelectedPodcastInteractor
    var selectedPodcast = Variable<Podcast>(Podcast())
    
    struct Input {
        let viewWillAppear: Driver<[Any]>
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        var episodeLists: Driver<[Episode]>?
        let selectedEpisode: Driver<Episode>?
    }
    
    func transform(input: Input) -> Output {
        
        
        let episodes = input.viewWillAppear.flatMap { [unowned self] _ in
            return self.getEpisodes(selectedPod: self.selectedPodcast.value).asDriver(onErrorJustReturn: [])
        }
        
        
        let selectedEpisode = input.selection.withLatestFrom(episodes) {($0, $1)}
            .filter({ (_, episodes) -> Bool in  return episodes.count > 0 })
            .map { (arg) -> Episode in
                let (index, episodes) = arg
                return episodes[index.row]
        }
        
        return Output(episodeLists: episodes, selectedEpisode: selectedEpisode)
    }
    
    

    @objc init(interactor: SelectedPodcastInteractor) {
        self.interactor = interactor
        super.init()
    }
    
    func getEpisodes(selectedPod: Podcast) -> Observable<[Episode]> {
        return self.interactor.getEpisodes(selectedPod: selectedPod)
    }
    
}

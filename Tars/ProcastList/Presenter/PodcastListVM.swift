//
//  PodcastListVM.swift
//  Tars
//
//  Created by yishain chen on 2017/12/17.
//  Copyright © 2017年 Ranger Techlologies. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PodcastListVM: NSObject {

    var interactor: PodcastListInteractor

    
    struct Input {
        let viewWillAppear: Driver<[Any]>
        let selection: Driver<IndexPath>
    }
    
    struct Output {
        var podcastLists: Driver<[Podcast]>?
        let selectedPodcast: Driver<Podcast>?
    }
    
    
    func transform(input: Input) -> Output {
        
        let podcasts = input.viewWillAppear.flatMap { _ in
            return self.getPodcasts().asDriver(onErrorJustReturn: [])
        }
    
        
        let selectedPodcast = input.selection.withLatestFrom(podcasts) {($0, $1)}
            .filter({ (_, podcasts) -> Bool in  return podcasts.count > 0 })
            .map { (arg) -> Podcast  in
                let (index, podcasts) = arg
                return podcasts[index.row]
            }
        
        return Output(podcastLists: podcasts, selectedPodcast: selectedPodcast)
    }
    
    @objc init(interactor: PodcastListInteractor) {
        self.interactor = interactor
        super.init()
    }
    
    func getPodcasts() ->  Observable<[Podcast]> {
        return self.interactor.getPodcasts()
    }
    
}

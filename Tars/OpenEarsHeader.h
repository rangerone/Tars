//
//  OpenEarsHeader.h
//  Tars
//
//  Created by yishain chen on 2017/12/31.
//  Copyright © 2017年 Ranger Techlologies. All rights reserved.
//

#ifndef OpenEarsHeader_h
#define OpenEarsHeader_h

#import <OpenEars/OEPocketsphinxController.h>
#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEFliteController.h>
#import <Slt/Slt.h>
#import <OpenEars/OEEventsObserver.h>
#import <OpenEars/OELogging.h>
#import <OpenEars/OEAcousticModel.h>

#endif /* OpenEarsHeader_h */


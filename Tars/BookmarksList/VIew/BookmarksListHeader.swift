
//
//  PodcastListHeader.swift
//  Tars
//
//  Created by yishain chen on 2017/12/16.
//  Copyright © 2017年 Ranger Technologies. All rights reserved.
//

import UIKit
import SnapKit

class BookmarksListHeader: UITableViewHeaderFooterView {
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = UIColor.white
        let titleLabel = UILabel(frame: CGRect.zero)
        titleLabel.font = UIFont.systemFont(ofSize: 14.0)
        titleLabel.textColor = UIColor.white
        titleLabel.lineBreakMode = .byTruncatingMiddle
        titleLabel.text = "52 Bookmarks"
        contentView.addSubview(titleLabel)
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self)
            make.left.equalTo(self).offset(10.0)
            make.height.equalTo(self)
            make.width.equalTo(170.0)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


import UIKit
import NVActivityIndicatorView

extension UIViewController {
    func topViewController() -> UIViewController? {
        return childViewControllers.last
    }
}

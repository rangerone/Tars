//
//  AppDelegate.swift
//  Tars
//
//  Created by yishain chen on 2017/12/10.
//  Copyright © 2017年 Ranger Technologies. All rights reserved.
//

import UIKit
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navigationController: UINavigationController!
    var tabBarController: TabBarController!
    var loginVC: LoginVC!
    var podcastListVC: PodcastListVC!
    var bookmarksListVC: BookmarksListVC!
    var exploreVC: ExploreVC!
    var episodePlayerVC: EpisodePlayerVC!
    
    var allPodcasts = [Podcast]() {
        didSet{
            print("FirsCall: \(self.allPodcasts.count)")
        }
    }
    
    var loginVCNavigationController: UINavigationController!
    var podcastVCNavigationController: UINavigationController!
    var bookmarksListVCNavigationController: UINavigationController!
    var exploreVCNavigationController: UINavigationController!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        loginVC = LoginVC()
        
        let podcastInteractor = PodcastListInteractor()
        let podcastListVM = PodcastListVM(interactor: podcastInteractor)
        podcastListVC = PodcastListVC(podcastListVM: podcastListVM)
        episodePlayerVC = EpisodePlayerVC()
        
        bookmarksListVC = BookmarksListVC()
        exploreVC = ExploreVC()
        
        loginVCNavigationController = NavigationController(rootViewController: loginVC)
        podcastVCNavigationController = NavigationController(rootViewController: podcastListVC)
        exploreVCNavigationController = NavigationController(rootViewController: exploreVC)
        bookmarksListVCNavigationController = NavigationController(rootViewController: bookmarksListVC)
        
        
        // Tab bar controller
        tabBarController = TabBarController()
        tabBarController.transparentTabBarEnabled = true
        podcastVCNavigationController.navigationBar.isHidden = true
        exploreVCNavigationController.navigationBar.isHidden = true
        bookmarksListVCNavigationController.navigationBar.isHidden = true
        
        tabBarController.addTab(index: System.podcastListTab, rootViewController: podcastVCNavigationController, selectedImage: #imageLiteral(resourceName: "home_tab_bar_selected"), unselectedImage: #imageLiteral(resourceName: "home_tab_bar_unselected"))
        tabBarController.addTab(index: System.exploreTab, rootViewController: exploreVCNavigationController, selectedImage: #imageLiteral(resourceName: "explore_tab_bar_selected"), unselectedImage: #imageLiteral(resourceName: "explore_tab_bar_unselected"))
        tabBarController.addTab(index: System.bookmarksTab, rootViewController: bookmarksListVCNavigationController, selectedImage: #imageLiteral(resourceName: "search_tab_bar_selected"), unselectedImage: #imageLiteral(resourceName: "search_tab_bar_unselected"))
        
        loginVCNavigationController = UINavigationController(rootViewController: loginVC)
        loginVCNavigationController.setNavigationBarHidden(true, animated: false)
        
        window = UIWindow()
        window?.rootViewController = loginVCNavigationController
        window?.makeKeyAndVisible()
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if url.absoluteString.contains("googleusercontent.apps") && url.absoluteString.contains("oauth2callback") {
            return loginVC.handleSignIn(url: url, options: options)
        }
        
        return false
    }
    
    @objc func beginInterruption() {
        Player.sharedInstance.pause()
        print("interrupted")
    }
    
    func collapsePlayer(animated: Bool) {
        tabBarController.accessoryViewController?.collapseAccessoryViewController(animated: animated)
        tabBarController.showTabBar(animated: animated)
    }
    
    func expandPlayer(animated: Bool) {
        tabBarController.accessoryViewController?.expandAccessoryViewController(animated: true)
        tabBarController.hideTabBar(animated: true)
    }
    
    func showPlayer(animated: Bool) {
        if tabBarController.accessoryViewController == episodePlayerVC { return }
        tabBarController.addAccessoryViewController(accessoryViewController: episodePlayerVC)
        collapsePlayer(animated: false)
    }
    
    func didFinishAuthenticatingUser() {
        window?.rootViewController = tabBarController
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


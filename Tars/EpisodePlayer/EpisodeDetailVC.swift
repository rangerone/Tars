//
//  EpisodeDetailVC.swift
//  Tars
//
//  Created by yishain chen on 2018/1/6.
//  Copyright © 2018年 Ranger Techlologies. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class EpisodeDetailVC: UIViewController {
    
    var timeLabel = UILabel()
    var rangeSlider = CERangeSlider()
    
    var tapGesture = UITapGestureRecognizer()

    var episodePlayerVM: EpisodePlayerVM!
    var episode : Episode!
    
    var episodeCell = UIView(frame: CGRect.zero)
    var episodeNameLabel = UILabel()
    var episodeIconImageView = UIImageView()
    
    var selectedPodcastAlbumImage = UIImage()
    var episodeDetailScrollView = UIScrollView()
    var bookmarkButton = UIButton()
    var playButton = UIButton()
    var episodeName = UILabel()
    var episodeDetailInfo = UILabel()
    let disposeBag = DisposeBag()
    
    
    init(selectedEpisode: Episode, albumImage: UIImage) {
        episode = selectedEpisode
        selectedPodcastAlbumImage = albumImage
        super.init(nibName: nil, bundle: nil)
        self.setupLayout()
        self.bindViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black
        
        rangeSlider.addTarget(self, action: #selector(EpisodeDetailVC.sliderChanges(_:)), for: .valueChanged)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupLayout() {
        view.addSubview(episodeCell)
        
        rangeSlider.minimumValue = 0
        rangeSlider.maximumValue = 1
        
        episodeNameLabel.textColor = UIColor.white
        episodeNameLabel.numberOfLines = 0
        episodeCell.backgroundColor = UIColor.black
        
        timeLabel.text = self.episode.duration
        timeLabel.textAlignment = .center
        timeLabel.textColor = UIColor.white
        timeLabel.font = UIFont.boldSystemFont(ofSize: 15)
        
        episodeIconImageView.image = selectedPodcastAlbumImage
        episodeNameLabel.text = episode.title
        episodeCell.addSubview(rangeSlider)
        episodeCell.addSubview(timeLabel)
        episodeCell.addSubview(episodeIconImageView)
        episodeCell.addSubview(episodeNameLabel)
        
        
        view.addSubview(episodeDetailScrollView)
        episodeDetailScrollView.alwaysBounceVertical = true
        
        playButton.setImage(UIImage(named: "play"), for: .normal)
        bookmarkButton.setImage(UIImage(named: "astronaut"), for: .normal)
        
        episodeName.text = self.episode.title
        episodeName.numberOfLines = 0
        episodeName.textAlignment = .left
        episodeName.textColor = UIColor.white
        episodeName.font = UIFont.boldSystemFont(ofSize: 22)
        
        episodeDetailInfo.text = self.episode.podDescription
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 8
        let podDescription = self.episode.podDescription == nil ? "" : self.episode.podDescription
        let attributedString = NSMutableAttributedString(string: podDescription!)
      //  attributedString.addAttributes([NSAttributedStringKey.paragraphStyle: paragraphStyle], range: NSMakeRange(0, self.episode.podDescription!.count))
        episodeDetailInfo.attributedText = attributedString
        
        
        episodeDetailInfo.numberOfLines = 0
        episodeDetailInfo.textAlignment = .left
        episodeDetailInfo.textColor = UIColor.white
        episodeDetailInfo.font = UIFont.systemFont(ofSize: 17)
        
        episodeDetailScrollView.addSubview(bookmarkButton)
        episodeDetailScrollView.addSubview(playButton)
        episodeDetailScrollView.addSubview(episodeName)
        episodeDetailScrollView.addSubview(episodeDetailInfo)
        
        
        
        episodeCell.snp.makeConstraints { make in
            make.top.equalTo(view).offset(22)
            make.left.right.equalTo(view)
            make.height.equalTo(100)
        }
        
        rangeSlider.snp.makeConstraints {(make) -> Void in
            make.top.right.left.bottom.equalTo(episodeCell)
        }
        
        timeLabel.snp.makeConstraints { make in
            make.right.equalTo(episodeCell.snp.right).offset(-10)
            make.bottom.equalTo(episodeCell.snp.bottom).offset(-10)
            make.width.equalTo(100)
            make.height.equalTo(50)
        }
        
        episodeIconImageView.snp.makeConstraints { make in
            make.centerY.equalTo(episodeCell.snp.centerY)
            make.left.equalTo(episodeCell.snp.left).offset(10)
            make.height.width.equalTo(80)
        }
        
        episodeNameLabel.snp.makeConstraints { make in
            make.top.equalTo(episodeCell).offset(10)
            make.left.equalTo(episodeIconImageView.snp.right).offset(10)
            make.right.equalTo(episodeCell).offset(-10)
        }
        
        episodeDetailScrollView.snp.makeConstraints { make in
            make.top.equalTo(episodeCell.snp.bottom)
            make.left.right.equalTo(view)
            make.bottom.equalTo(view.snp.bottom).offset(-80)
        }
        
        bookmarkButton.snp.makeConstraints { make in
            make.top.equalTo(episodeDetailScrollView).offset(5)
            make.centerX.equalTo(view.frame.width/4)
            make.width.height.equalTo(100)
        }
        
        playButton.snp.makeConstraints { make in
            make.centerY.equalTo(bookmarkButton)
            make.centerX.equalTo(view.frame.width*3/4)
            make.width.height.equalTo(90)
        }
        
        episodeName.snp.makeConstraints { make in
            make.top.equalTo(bookmarkButton.snp.bottom).offset(10)
            make.left.equalTo(view).offset(50)
            make.right.equalTo(view).offset(-50)
        }
        
        episodeDetailInfo.snp.makeConstraints { make in
            make.top.equalTo(episodeName.snp.bottom).offset(10)
            make.left.equalTo(view).offset(40)
            make.right.equalTo(view).offset(-40)
            make.bottom.equalTo(episodeDetailScrollView)
        }
    }
    
    func bindViewModel() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.episodePlayerVC.delegate = self
        
        playButton.rx.tap
            .subscribe(onNext: { [unowned self] _ in
                appDelegate?.showPlayer(animated: true)
                
                Player.sharedInstance.playEpisode(episode: self.episode, episodeImage: self.selectedPodcastAlbumImage)
                if Player.sharedInstance.isPlaying {
                    self.playButton.setImage(UIImage(named: "pause"), for: .normal)
                } else {
                    self.playButton.setImage(UIImage(named: "play"), for: .normal)
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    @objc func sliderChanges(_ sender: UISlider) {
        
        let targetTime = CMTimeMakeWithSeconds(Float64(rangeSlider.upperValue * Float(Player.sharedInstance.currentItemDuration().seconds)), Player.sharedInstance.currentItemElapsedTime().timescale)
        
        Player.sharedInstance.seekTargetTime(targetTime)
        self.timeLabel.text = Player.sharedInstance.currentItemElapsedTime().descriptionText
    }
    
    //繪出bookmark的點，需要再改一下內容實作
    func drawProgressLayer() {
        let bookmarkRect = CGRect(x: CGFloat(rangeSlider.upperValue) * self.view.frame.size.width - 10, y: 0, width:  CGFloat(10), height: 45)
        let bezierPath = UIBezierPath(roundedRect: bookmarkRect, cornerRadius: 0)
        bezierPath.close()
        let bookmarkLayer : CAShapeLayer = CAShapeLayer()
        bookmarkLayer.path = bezierPath.cgPath
        bookmarkLayer.fillColor = UIColor.orange.cgColor
        bookmarkLayer.strokeEnd = 0
    //    progressBar.layer.addSublayer(bookmarkLayer)
        
    }

}

extension EpisodeDetailVC: EpisodePlayerDelegate {
    func viewDidTapExpandButton() {
        if Player.sharedInstance.isPlaying {
            self.playButton.setImage(UIImage(named: "pause"), for: .normal)
        } else {
            self.playButton.setImage(UIImage(named: "play"), for: .normal)
        }
    }
    
    func updateUIForPlayback() {
        rangeSlider.upperValue = Float(Player.sharedInstance.getProgress())
        self.timeLabel.text = Player.sharedInstance.currentItemElapsedTime().descriptionText
    }

}


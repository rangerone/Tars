//
//  EpisodePlayerVC.swift
//  Tars
//
//  Created by yishain chen on 2017/12/24.
//  Copyright © 2017年 Ranger Techlologies. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import RxSwift
import RxCocoa

protocol EpisodePlayerDelegate: class {
    func updateUIForPlayback()
    func viewDidTapExpandButton()
}


class EpisodePlayerVC: TabBarAccessoryViewController ,UIGestureRecognizerDelegate  {
    

    var episodePlayerVM: EpisodePlayerVM!
    var miniPlayerView: MiniPlayerView!
    var playerHeaderView: PlayerHeaderView!
    var artworkImage = UIImageView()
    var timeLabel = UILabel()
    var playButton = UIButton()
    var episodeName = UILabel()
    var bookmarkButton = UIButton()
    var rewindButton = UIButton()
    var fastForwardButton = UIButton()
    var progressBar = UIView()
    var isCollapsed: Bool = false
    var rangeSlider = CERangeSlider()
    
    weak var delegate: EpisodePlayerDelegate?
    
    var interactor: Interactor? = nil
    let disposeBag = DisposeBag()
    
    var slt = Slt()
    var openEarsEventsObserver = OEEventsObserver()
    var fliteController = OEFliteController()
    
    var pathToFirstDynamicallyGeneratedLanguageModel: String!
    var pathToFirstDynamicallyGeneratedDictionary: String!
    var restartAttemptsDueToPermissionRequests = Int()
    var startupFailedDueToLackOfPermissions = Bool()
    var usingStartingLanguageModel = Bool()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.setupLayout()
        self.bindViewModel()
        self.setupOpenEarsModule()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black

        rangeSlider.addTarget(self, action: #selector(EpisodePlayerVC.sliderChanges(_:)), for: .valueChanged)
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setupLayout() {
        
        episodeName.numberOfLines = 2
        episodeName.textAlignment = .center
        episodeName.textColor = UIColor.white
        episodeName.font = UIFont.boldSystemFont(ofSize: 25)
        
        timeLabel.textAlignment = .center
        timeLabel.textColor = UIColor.white
        timeLabel.font = UIFont.boldSystemFont(ofSize: 30)
        
        progressBar.backgroundColor = UIColor.lightGray
        progressBar.alpha = 0.8
        
        rangeSlider.minimumValue = 0
        rangeSlider.maximumValue = 1
        
        playButton.setImage(UIImage(named: "play"), for: .normal)
        rewindButton.setImage(UIImage(named: "rewind"), for: .normal)
        fastForwardButton.setImage(UIImage(named: "forward"), for: .normal)
        bookmarkButton.setImage(UIImage(named: "astronaut"), for: .normal)
        
        view.addSubview(rangeSlider)
        view.addSubview(artworkImage)
        view.addSubview(episodeName)
        view.addSubview(timeLabel)
        view.addSubview(progressBar)
        view.addSubview(playButton)
        view.addSubview(rewindButton)
        view.addSubview(fastForwardButton)
        view.addSubview(bookmarkButton)
        
        miniPlayerView = MiniPlayerView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 0))
        miniPlayerView.frame.size.width = view.frame.width
        miniPlayerView.delegate = self
        view.addSubview(miniPlayerView)
        
        playerHeaderView = PlayerHeaderView(frame: .zero)
        playerHeaderView.frame.size.width = view.frame.width
        playerHeaderView.delegate = self
        playerHeaderView.alpha = 0.0
        view.addSubview(playerHeaderView)
        
        rangeSlider.snp.makeConstraints {(make) -> Void in
            make.top.right.left.bottom.equalTo(view)
        }
        
        artworkImage.snp.makeConstraints {(make) -> Void in
            make.top.equalTo(view).offset(30)
            make.centerX.equalTo(view)
            make.height.width.equalTo(150)
        }
        
        episodeName.snp.makeConstraints {(make) -> Void in
            make.top.equalTo(artworkImage.snp.bottom).offset(10)
            make.centerX.equalTo(view)
            make.height.equalTo(80)
            make.width.equalTo(250)
        }
        
        timeLabel.snp.makeConstraints {(make) -> Void in
            make.top.equalTo(episodeName.snp.bottom).offset(10)
            make.centerX.equalTo(view)
            make.height.equalTo(45)
            make.width.equalTo(150)
        }
        
        progressBar.snp.makeConstraints {(make) -> Void in
            make.top.equalTo(timeLabel.snp.bottom).offset(10)
            make.left.right.equalTo(view)
            make.height.equalTo(45)
        }
        
        
        playButton.snp.makeConstraints {(make) -> Void in
            make.top.equalTo(progressBar.snp.bottom).offset(50)
            make.centerX.equalTo(view)
            make.height.width.equalTo(80)
        }
        
        rewindButton.snp.makeConstraints { (make) -> Void in
            make.centerY.equalTo(playButton)
            make.right.equalTo(playButton.snp.left).offset(-30)
            make.height.width.equalTo(50)
        }
        
        fastForwardButton.snp.makeConstraints {(make) -> Void in
            make.centerY.equalTo(playButton)
            make.left.equalTo(playButton.snp.right).offset(30)
            make.height.width.equalTo(50)
        }
        
        bookmarkButton.snp.makeConstraints {(make) -> Void in
            make.bottom.equalTo(view).offset(-80)
            make.centerX.equalTo(view)
            make.height.equalTo(40)
            make.width.equalTo(50)
        }
        
        
    }
    
    func bindViewModel() {
        playButton.rx.tap
            .subscribe(onNext: { [unowned self] _ in
                Player.sharedInstance.togglePlaying()
                self.delegate?.viewDidTapExpandButton()
                if Player.sharedInstance.isPlaying {
                    self.startRecording()
                    self.playButton.setImage(UIImage(named: "pause"), for: .normal)
                } else {
                    self.stopRecording()
                    self.playButton.setImage(UIImage(named: "play"), for: .normal)
                }
            })
            .disposed(by: disposeBag)
        
        rewindButton.rx.tap
            .subscribe(onNext: { _ in
                // back 15 secs
                Player.sharedInstance.skip(seconds: -15)
            })
            .disposed(by: disposeBag)
        
        fastForwardButton.rx.tap
            .subscribe(onNext: {  _ in
                Player.sharedInstance.skip(seconds: 15)
            })
            .disposed(by: disposeBag)
        
        bookmarkButton.rx.tap
            .subscribe(onNext: { [unowned self] _ in
                self.bookmarkAction()
            })
            .disposed(by: disposeBag)
        
        Player.sharedInstance.delegate = self
        
    }
    
    @objc func sliderChanges(_ sender: UISlider) {

        let targetTime = CMTimeMakeWithSeconds(Float64(rangeSlider.upperValue * Float(Player.sharedInstance.currentItemDuration().seconds)), Player.sharedInstance.currentItemElapsedTime().timescale)
      
        Player.sharedInstance.seekTargetTime(targetTime)
        self.timeLabel.text = Player.sharedInstance.currentItemElapsedTime().descriptionText
        
    }
    
    func bookmarkAction() {
        //點擊或透過語音bookmark後，需要繪製座標點以及上傳bookmark座標點
        self.drawProgressLayer()
        self.uploadBookmarkPoint()
    }
    
    func uploadBookmarkPoint() {
        
    }
    
    //繪出bookmark的點，需要再改一下內容實作
    func drawProgressLayer() {
        let bookmarkRect = CGRect(x: CGFloat(rangeSlider.upperValue) * self.view.frame.size.width - 10, y: 0, width:  CGFloat(10), height: 45)
        let bezierPath = UIBezierPath(roundedRect: bookmarkRect, cornerRadius: 0)
        bezierPath.close()
        let bookmarkLayer : CAShapeLayer = CAShapeLayer()
        bookmarkLayer.path = bezierPath.cgPath
        bookmarkLayer.fillColor = UIColor.orange.cgColor
        bookmarkLayer.strokeEnd = 0
        progressBar.layer.addSublayer(bookmarkLayer)
        
    }
    
    private func startRecording() {
        if(!OEPocketsphinxController.sharedInstance().isListening) {
            OEPocketsphinxController.sharedInstance().startListeningWithLanguageModel(atPath: self.pathToFirstDynamicallyGeneratedLanguageModel, dictionaryAtPath: self.pathToFirstDynamicallyGeneratedDictionary, acousticModelAtPath: OEAcousticModel.path(toModel: "AcousticModelEnglish"), languageModelIsJSGF: false) // Start speech recognition, but only if we aren't already listening.
        }
    }
    
    private func stopRecording() {
        if(OEPocketsphinxController.sharedInstance().isListening) { // If we're listening, stop listening.
            let stopListeningError: Error! = OEPocketsphinxController.sharedInstance().stopListening()
            if(stopListeningError != nil) {
                print("Error while stopping listening in testRecognitionCompleted: \(stopListeningError)")
            }
        }
    }
    
    func setupOpenEarsModule()  {
        
        self.openEarsEventsObserver.delegate = self
        self.restartAttemptsDueToPermissionRequests = 0
        self.startupFailedDueToLackOfPermissions = false
        
        let languageModelGenerator = OELanguageModelGenerator()
        let firstLanguageArray = ["tars"]
        let firstVocabularyName = "FirstVocabulary"
        let firstLanguageModelGenerationError: Error! = languageModelGenerator.generateLanguageModel(from: firstLanguageArray, withFilesNamed: firstVocabularyName, forAcousticModelAtPath: OEAcousticModel.path(toModel: "AcousticModelEnglish")) // Change "AcousticModelEnglish" to "AcousticModelSpanish" in order to create a language model for Spanish recognition instead of English.
        
        if(firstLanguageModelGenerationError != nil) {
            print("Error while creating initial language model: \(firstLanguageModelGenerationError)")
        } else {
            self.pathToFirstDynamicallyGeneratedLanguageModel = languageModelGenerator.pathToSuccessfullyGeneratedLanguageModel(withRequestedName: firstVocabularyName) // these are convenience methods you can use to reference the file location of a language model that is known to have been created successfully.
            self.pathToFirstDynamicallyGeneratedDictionary = languageModelGenerator.pathToSuccessfullyGeneratedDictionary(withRequestedName: firstVocabularyName) // these are convenience methods you can use to reference the file location of a dictionary that is known to have been created successfully.
            self.usingStartingLanguageModel = true // Just keeping track of which model we're using.
        }
    }
    
    func expand() {
        
        miniPlayerView.alpha = 0.0
        artworkImage.alpha = 1.0
        playerHeaderView.alpha = 1.0
        view.frame.origin.y = 0
        UIApplication.shared.isStatusBarHidden = true
        
        isCollapsed = false
    }
    
    func collapse() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        miniPlayerView.alpha = 1.0
        artworkImage.alpha = 0.0
        playerHeaderView.alpha = 0.0
        view.frame.origin.y = self.view.frame.height - appDelegate.tabBarController.tabBarHeight - self.miniPlayerView.frame.height
        UIApplication.shared.isStatusBarHidden = false
        
        isCollapsed = true
    }
    
    func animatePlayer(animations: @escaping () -> Void, completion: ((Bool) -> Void)?) {
        UIView.animate(withDuration: 0.3, animations: animations, completion: completion)
    }
    
    override func accessoryViewFrame() -> CGRect? {
        return miniPlayerView.frame
    }
    
    override func showAccessoryViewController(animated: Bool) {
        if animated {
            animatePlayer(animations: {
                self.view.alpha = 1.0
            }, completion: nil)
        } else {
            view.alpha = 1.0
        }
    }
    
    override func hideAccessoryViewController(animated: Bool) {
        if animated {
            animatePlayer(animations: {
                self.view.alpha = 0.0
            }, completion: nil)
        } else {
            view.alpha = 0.0
        }
    }
    
    override func expandAccessoryViewController(animated: Bool) {
        guard isCollapsed else { return }
        
        self.view.alpha = 1.0
    
        if animated {
            animatePlayer(animations: {
                self.expand()
            }, completion: nil)
        } else {
            self.expand()
        }
    }
    
    override func collapseAccessoryViewController(animated: Bool) {
        guard !isCollapsed else { return }
        
        if animated {
            animatePlayer(animations: {
                self.collapse()
            }, completion: { _ in
            })
        } else {
            self.collapse()
        }
    }
    
}

extension EpisodePlayerVC: MiniPlayerViewDelegate {
    func miniPlayerViewDidDrag(sender: UIPanGestureRecognizer) {
        
    }
    
    func miniPlayerViewDidTapPlayPauseButton() {
        delegate?.viewDidTapExpandButton()
        Player.sharedInstance.togglePlaying()
        if Player.sharedInstance.isPlaying {
            self.playButton.setImage(UIImage(named: "pause"), for: .normal)
        } else {
            self.playButton.setImage(UIImage(named: "play"), for: .normal)
        }
    }
    
    func miniPlayerViewDidTapExpandButton() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        appDelegate.expandPlayer(animated: true)
    }
}

extension EpisodePlayerVC: PlayerHeaderViewDelegate {
    func playerHeaderViewDidTapCollapseButton() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        appDelegate.collapsePlayer(animated: true)
    }
    
    func playerHeaderViewDidDrag(sender: UIPanGestureRecognizer) {
        
    }
    
    
}

extension EpisodePlayerVC: PlayerDelegate {
    func updateUIForEpisode(episode: Episode, episodeImage: UIImage) {
        miniPlayerView.updateUIForEpisode(episode: episode)
        artworkImage.image = episodeImage
        episodeName.text = episode.title
        timeLabel.text = episode.duration
        if Player.sharedInstance.isPlaying {
            self.playButton.setImage(UIImage(named: "pause"), for: .normal)
        } else {
            self.playButton.setImage(UIImage(named: "play"), for: .normal)
        }
    }
    
    func updateUIForPlayback() {
        let player = Player.sharedInstance
        miniPlayerView.updateUIForPlayback(isPlaying: player.isPlaying)
        delegate?.updateUIForPlayback()
        rangeSlider.upperValue = Float(Player.sharedInstance.getProgress())
        self.timeLabel.text = Player.sharedInstance.currentItemElapsedTime().descriptionText
        if Player.sharedInstance.isPlaying {
            self.playButton.setImage(UIImage(named: "pause"), for: .normal)
        } else {
            self.playButton.setImage(UIImage(named: "play"), for: .normal)
        }
    }
    
    func updateUIForEmptyPlayer() {
        miniPlayerView.updateUIForEmptyPlayer()
    }
}

extension EpisodePlayerVC: OEEventsObserverDelegate {
    
    func micPermissionCheckCompleted(withResult: Bool) {
        if(withResult) {
            
            self.restartAttemptsDueToPermissionRequests += 1
            if(self.restartAttemptsDueToPermissionRequests == 1 && self.startupFailedDueToLackOfPermissions) { // If we get here because there was an attempt to start which failed due to lack of permissions, and now permissions have been requested and they returned true, we restart exactly once with the new permissions.
                
                if(!OEPocketsphinxController.sharedInstance().isListening) {
                    OEPocketsphinxController.sharedInstance().startListeningWithLanguageModel(atPath: self.pathToFirstDynamicallyGeneratedLanguageModel, dictionaryAtPath: self.pathToFirstDynamicallyGeneratedDictionary, acousticModelAtPath: OEAcousticModel.path(toModel: "AcousticModelEnglish"), languageModelIsJSGF: false) // Start speech recognition, but only if we aren't already listening.
                }
                
            }
        }
        self.startupFailedDueToLackOfPermissions = false
    }
    
    func pocketsphinxRecognitionLoopDidStart() {
        
        print("Local callback: Pocketsphinx started.") // Log it.
        
    }
    
    // An optional delegate method of OEEventsObserver which informs that Pocketsphinx is now listening for speech.
    func pocketsphinxDidStartListening() {
        
        print("Local callback: Pocketsphinx is now listening.") // Log it.
        
    }
    
    func audioInputDidBecomeAvailable() {
        print("Local callback: The audio input is available") // Log it.
        if(!OEPocketsphinxController.sharedInstance().isListening) {
            OEPocketsphinxController.sharedInstance().startListeningWithLanguageModel(atPath: self.pathToFirstDynamicallyGeneratedLanguageModel, dictionaryAtPath: self.pathToFirstDynamicallyGeneratedDictionary, acousticModelAtPath: OEAcousticModel.path(toModel: "AcousticModelEnglish"), languageModelIsJSGF: false) // Start speech recognition, but only if we aren't already listening.
        }
    }
    // An optional delegate method of OEEventsObserver which informs that there was a change to the audio route (e.g. headphones were plugged in or unplugged).
    func audioRouteDidChange(toRoute newRoute: String!) {
        print("Local callback: Audio route change. The new audio route is \(newRoute)") // Log it.
        let stopListeningError: Error! = OEPocketsphinxController.sharedInstance().stopListening() // React to it by telling Pocketsphinx to stop listening since there is no available input (but only if we are listening).
        if(stopListeningError != nil) {
            print("Error while stopping listening in audioInputDidBecomeAvailable: \(stopListeningError)")
        }
    }
    
    func pocketsphinxDidReceiveHypothesis(_ hypothesis: String!, recognitionScore: String!, utteranceID: String!) {
        print("Local callback: The received hypothesis is \(hypothesis!) with a score of \(recognitionScore!) and an ID of \(utteranceID!)") // Log it.
        
        if(hypothesis! == "tars") {
            self.bookmarkAction()
        }
        
        if(hypothesis! == "change model") { // If the user says "change model", we will switch to the alternate model (which happens to be the dynamically generated model).
            
            // Here is an example of language model switching in OpenEars. Deciding on what logical basis to switch models is your responsibility.
            // For instance, when you call a customer service line and get a response tree that takes you through different options depending on what you say to it,
            // the models are being switched as you progress through it so that only relevant choices can be understood. The construction of that logical branching and
            // how to react to it is your job OpenEars just lets you send the signal to switch the language model when you've decided it's the right time to do so.
            
            if(self.usingStartingLanguageModel) { // If we're on the starting model, switch to the dynamically generated one.
                OEPocketsphinxController.sharedInstance().changeLanguageModel(toFile: self.pathToFirstDynamicallyGeneratedLanguageModel, withDictionary:self.pathToFirstDynamicallyGeneratedDictionary)
                self.usingStartingLanguageModel = false
                
            } else { // If we're on the dynamically generated model, switch to the start model (this is an example of a trigger and method for switching models).
                OEPocketsphinxController.sharedInstance().changeLanguageModel(toFile: self.pathToFirstDynamicallyGeneratedLanguageModel, withDictionary:self.pathToFirstDynamicallyGeneratedDictionary)
                self.usingStartingLanguageModel = true
            }
        }
        
        
        // This is how to use an available instance of OEFliteController. We're going to repeat back the command that we heard with the voice we've chosen.
        //  self.fliteController.say(_:"You said \(hypothesis!)", with:self.slt)
    }
    
    func pocketSphinxContinuousSetupDidFail(withReason reasonForFailure: String!) { // This can let you know that something went wrong with the recognition loop startup. Turn on [OELogging startOpenEarsLogging] to learn why.
        print("Local callback: Setting up the continuous recognition loop has failed for the reason \(reasonForFailure), please turn on OELogging.startOpenEarsLogging() to learn more.") // Log it.
        
    }
    
    func pocketSphinxContinuousTeardownDidFail(withReason reasonForFailure: String!) { // This can let you know that something went wrong with the recognition loop startup. Turn on [OELogging startOpenEarsLogging] to learn why.
        print("Local callback: Tearing down the continuous recognition loop has failed for the reason %, please turn on [OELogging startOpenEarsLogging] to learn more.", reasonForFailure) // Log it.
    }
    
    func testRecognitionCompleted() { // A test file which was submitted for direct recognition via the audio driver is done.
        print("Local callback: A test file which was submitted for direct recognition via the audio driver is done.") // Log it.
        if(OEPocketsphinxController.sharedInstance().isListening) { // If we're listening, stop listening.
            let stopListeningError: Error! = OEPocketsphinxController.sharedInstance().stopListening()
            if(stopListeningError != nil) {
                print("Error while stopping listening in testRecognitionCompleted: \(stopListeningError)")
            }
        }
        
    }
    /** Pocketsphinx couldn't start because it has no mic permissions (will only be returned on iOS7 or later).*/
    func pocketsphinxFailedNoMicPermissions() {
        print("Local callback: The user has never set mic permissions or denied permission to this app's mic, so listening will not start.")
        self.startupFailedDueToLackOfPermissions = true
        if(OEPocketsphinxController.sharedInstance().isListening){
            let stopListeningError: Error! = OEPocketsphinxController.sharedInstance().stopListening()
            if(stopListeningError != nil) {
                print("Error while stopping listening in pocketsphinxFailedNoMicPermissions: \(stopListeningError). Will try again in 10 seconds.")
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
            if(!OEPocketsphinxController.sharedInstance().isListening) {
                OEPocketsphinxController.sharedInstance().startListeningWithLanguageModel(atPath: self.pathToFirstDynamicallyGeneratedLanguageModel, dictionaryAtPath: self.pathToFirstDynamicallyGeneratedDictionary, acousticModelAtPath: OEAcousticModel.path(toModel: "AcousticModelEnglish"), languageModelIsJSGF: false) // Start speech recognition, but only if we aren't already listening.
            }
        })
    }
}

